package controller;

public class Book extends BookModel {
	//data members
	private String ISBN;
	private double price;
	private String author;
	private int edition;
	//constructor
	public Book(String id, String title, String publisher, String yearPublished, boolean status, String iSBN, double price, String author, int edition) {
		super(id, title, publisher, yearPublished, status);
		ISBN = iSBN;
		this.price = price;
		this.author = author;
		this.edition = edition;
	}
	//toString method
	@Override
	public String toString() {	
		return super.toString() + "\t" + ISBN + "\t" + price + "\t" + author + "\t" + edition;
	}
	//end of class
}
