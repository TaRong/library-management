package model;

import java.util.Scanner;

import controller.Book;
import controller.BookModel;
import controller.Library;
import controller.Member;
import controller.Thesis;
import controller.Transaction;

public class ConsoleFunctions {
	//library object
	public static Library library = new Library();
	//scanner
	public static Scanner scanner = new Scanner(System.in);
	
	//=============================== About Us  ==================================
	public static void aboutUs() {
		System.out.println("  ┌──────── ABOUT US ────────────────────────────────────────────────┐");
		System.out.println("  │              ╔══════════════════════════════╗                    │");
		System.out.println("  │              ║  Basic Java Programming      ║                    │");
		System.out.println("  │              ║  Teaches by:                 ║                    │");
		System.out.println("  │              ║  Teacher THAY SETHA          ║                    │");
		System.out.println("  │              ╚══════════════════════════════╝                    │");
		System.out.println("  │              ╔══════════════════════════════╗                    │");
		System.out.println("  │              ║          Group Member        ║                    │");
		System.out.println("  │              ╠══════════════════════════════╣                    │");
		System.out.println("  │              ║    1 HONG SETHARONG          ║                    │");
		System.out.println("  │              ║    2 HAYTHAI SAKADARITH      ║                    │");
		System.out.println("  │              ║    3 IN   ANTRA              ║                    │");
		System.out.println("  │              ║    4 KHUY SOKHKET            ║                    │");
		System.out.println("  │              ║    5 POU  LUCH               ║                    │");
		System.out.println("  │              ╚══════════════════════════════╝                    │");
		System.out.println("  └──────────────────────────────────────────────────────────────────┘");
	}
	// ============================== Main Menu ==================================
	public static void mainMenus() {
		boolean isContinues = true;
		do {
			//main menu output
			System.out.println("        ╔═════════════════════════════════════════════╗");
			System.out.println("        ║         LIBRARY MANAGEMENT SYSTEM           ║");
			System.out.println("        ╚═════════════════════════════════════════════╝");
			System.out.println("              ╔═════════════════════════════════╗");
			System.out.println("              ║               MENU              ║");
			System.out.println("              ╠═════════════════════════════════╣");
			System.out.println("              ║   1, Add new book.              ║");
			System.out.println("              ║   2, Add new thesis.            ║");
			System.out.println("              ║   3, Add new member.            ║");
			System.out.println("              ║   4, Borrow a book.             ║");
			System.out.println("              ║   5, Return a book.             ║");
			System.out.println("              ║   6, List members.              ║");
			System.out.println("              ║   7, List books by category.    ║");
			System.out.println("              ║   8, List of borrowed books.    ║");
			System.out.println("              ║   Other, Exit.                  ║");
			System.out.println("              ╚═════════════════════════════════╝");
			System.out.print  ("                  Your choice : "                 );
			//switch the choice
			switch (scanner.nextLine()) {
				case "1":
					do {
						try {
							addNewBook();
							System.out.print("Do you want to add more book? (Y/N) ");
						}catch (NumberFormatException e) {
							System.out.print("Incorrectes input, Try again? (Y/N) ");
						}
					}while(scanner.nextLine().equalsIgnoreCase("y"));
					break;
				case "2":
					do {
						addNewThesis();
						System.out.print("Do you want to add more thesis? (Y/N) ");
					}while(scanner.nextLine().equalsIgnoreCase("y"));
					break;
				case "3":
					do {
						addNewMember();
						System.out.println("Do you want to add more member? (Y/N) ");
					}while(scanner.nextLine().equalsIgnoreCase("y"));
					break;
				case "4":
					do {
						borrowesBook();
						System.out.print("Do you want to add more transaction? (Y/N) ");
					}while(scanner.nextLine().equalsIgnoreCase("y"));
					break;
				case "5":
					do {
						try {
							returnsBook();
							System.out.print("Do you want to return more book? (Y/N) ");
						}catch (NumberFormatException e) {
							System.out.print("Incorrectes input, Try again? (Y/N) ");
						}
					}while(scanner.nextLine().equalsIgnoreCase("y"));
				case "6":
					memberList();
					scanner.nextLine();
					break;
				case "7":
					bookList();
					scanner.nextLine();
					break;
				case "8":
					borrowedBookList();
					scanner.nextLine();
					break;
				default:
					isContinues = false;
					break;
			}
			
		}while(isContinues);
	}
	// =========================== Add new book ==================================	
	public static void addNewBook() throws NumberFormatException {
		//input book info
		System.out.println("──────────────────────────── Add New Book ────────────────────────────\n");
		System.out.println("Enter book id:"				); String bookID = dataChecker(scanner.nextLine(), "Book");
		if(bookID == null) return; //terminates this progress
		System.out.println("Enter book title:"			); String bookTitle = scanner.nextLine();
		System.out.println("Enter book publisher:"		); String bookPublisher = scanner.nextLine();
		System.out.println("Enter book published year:"	); String bookPublishedYear = scanner.nextLine();
		System.out.println("Enter book ISBN:"			); String bookISBN = scanner.nextLine();
		System.out.println("Enter book price(number):"	); double bookPrice = Double.parseDouble(scanner.nextLine());
		System.out.println("Enter book author:"			); String bookAuthor = scanner.nextLine();
		System.out.println("Enter book edition(number):"); int bookEdition = Integer.parseInt(scanner.nextLine());
		//add book to library by constructor
		library.addBook(new Book(bookID, bookTitle, bookPublisher,bookPublishedYear, true, bookISBN, bookPrice,bookAuthor, bookEdition));  
		System.out.println("────────────────────────────────────────────────────────");
		System.out.println("Book : " + bookTitle + " was successfully added.");
		System.out.println("────────────────────────────────────────────────────────");
	}
	// =========================== Add new thesis ================================
	public static void addNewThesis() {
		//input thesis info
		System.out.println("──────────────────────────── Add New Thesis ────────────────────────────\n");
		System.out.print("Enter thesis id: "			); String thesisID = dataChecker(scanner.nextLine(), "Thesis");
		if(thesisID == null) return; //terminates this progress
		System.out.println("Enter thesis title: "			); String thesisTitle = scanner.nextLine();
		System.out.println("Enter thesis publisher: "		); String thesisPublisher = scanner.nextLine();
		System.out.println("Enter thesis published year: "); String thesisPublishedYear = scanner.nextLine();
		System.out.println("Enter thesis writer: "		); String thesisWriter = scanner.nextLine();
		System.out.println("Enter type of thesis: "		); String thesisType = scanner.nextLine();
		//add thesis to library
		library.addBook(new Thesis(thesisID, thesisTitle, thesisPublisher,thesisPublishedYear, true, thesisWriter, thesisType));
		System.out.println("────────────────────────────────────────────────────────");
	  	System.out.println("Thesis : " + thesisTitle + " was successfully added.");
	  	System.out.println("────────────────────────────────────────────────────────");
	}
	// =========================== Add new member ================================	
	public static void addNewMember() {
		//input member info
		System.out.println("───────────────────────────────────── ADD NEW MEMBER ────────────────────────────────\n");
		System.out.println("Enter member id: "			); String memberID = dataChecker(scanner.nextLine(), "Member");
		System.out.println("Enter member name: "			); String memberName = scanner.nextLine();
		if(memberID == null) return; //terminates this progress
		System.out.println("Enter member address: "		); String memberAddress = scanner.nextLine();
		System.out.println("Enter date of membership: "	); String dateOfMembership = scanner.nextLine();
		System.out.println("Enter type of membership: "	); char typeMembership = scanner.nextLine().charAt(0);
		//add member to library
		library.addMember(new Member(memberID, memberName, memberAddress, dateOfMembership, typeMembership));
		System.out.println("────────────────────────────────────────────────────────────────────────────────────");
		System.out.println("Member : " + memberName + " was successfully added.");
		System.out.println("────────────────────────────────────────────────────────────────────────────────────");
	}
	// =========================== Borrow a book =================================	
	public static void borrowesBook() {
		System.out.println("───────────────────────────────────── Borrow a book ────────────────────────────────\n");
		System.out.println("Enter member id:"			); String memberID = scanner.nextLine();
		//check if this memberID exist
		Member member = library.getMember(memberID);
		if(member == null) {
			System.out.println("Member with ID " + memberID + " is not found!");
			System.out.println("\n────────────────────────────────────────────────────────────────────────────────────");
			return; //terminates this progress
		}
		System.out.println("Enter book id:"				); String bookID = scanner.nextLine();
		//check if this bookID exist
		BookModel bookModel = library.getBook(bookID);
		if(bookModel == null) {
			System.out.println("Book with ID " + bookID + " is not found!");
			System.out.println("\n────────────────────────────────────────────────────────────────────────────────────");
			return; //terminates this progress
		}else if(bookModel.getStatus() == false) { //checking if this book not available
			System.out.println("Book with ID " + bookID + " is not available!");
			System.out.println("\n────────────────────────────────────────────────────────────────────────────────────");
			return; //terminates this progress
		}
		System.out.println("Enter date of issue:"		); String dateOfIssue = scanner.nextLine();
		System.out.println("Enter due date:"			); String dueDate = scanner.nextLine();
		//add transaction to library
		library.addTransaction(new Transaction(library.getNextTID(), member, bookModel, dateOfIssue, dueDate));
		System.out.println("You have successfully borrowed book " + bookModel.getTitle());
		System.out.println("\n────────────────────────────────────────────────────────────────────────────────────");
	}
	
	// =========================== Return a book =================================	
	public static void returnsBook() throws NumberFormatException {
		System.out.println("───────────────────────────────────── Return a book ────────────────────────────────\n");
		System.out.println("Enter transaction id(number):"); int tID = Integer.parseInt(scanner.nextLine());
		//remove transaction from the library
		try {
			Transaction t = library.getTransaction(tID);
			library.removeTransaction(t);
			System.out.println("You have successfully returned book " + t.getBook().getTitle());
		}catch(NullPointerException e) {
			System.out.println("Transaction with ID " + tID + " is not found!");
		}finally {
			System.out.println("\n────────────────────────────────────────────────────────────────────────────────────");			
		}
	}
	// =========================== List members ==================================
	public static void memberList() {
		System.out.println("───────────────────────────────────── Member List ────────────────────────────────\n");
		System.out.println("[ID\tName\t\tAddress\tDate\tMembership]");
		System.out.println("──────────────────────────────────────────────────────────────");
		for(Member member:library.getAllMembers()) {
			System.out.println(member.toString());
		}
		System.out.println("────────────────────────────────────────────────────────────────────────────────");
	}
	// =========================== List books by category ========================	
	public static void bookList() {
		int totalBook = library.getAllBooks().size();
		int borrowedBooks = library.getAllTransactions().size();
		
		System.out.println("───────────────────────────────────── Book List ────────────────────────────────\n");
		System.out.println("Total Book : " + totalBook + ", Borrowed: " + borrowedBooks
					+ ", available: " + (totalBook - borrowedBooks));
		System.out.println("────────────────────────────────────────────────────────────────────────────────\n");
		System.out.println("[ID\tTitle\t\tPublisher\tPublishedYear\tStatus\tISBN\tPrice\tAuthor\tEdition]");
		System.out.println("────────────────────────────────────────────────────────────────────────────────");
		for(BookModel bookModel:library.getAllBooks()) {
			if(bookModel instanceof Book) {
				System.out.println(bookModel.toString());
			}
		}
		System.out.println("\n────────────────────────────────────────────────────────────────────────────────\n");
		System.out.println("[ID\tTitle\t\tPublisher\tPublishedYear\tStatus\tWriter\tType]");
		System.out.println("────────────────────────────────────────────────────────────────────────────────");
		for(BookModel bookModel:library.getAllBooks()) {
			if(bookModel instanceof Thesis) {
				System.out.println(bookModel.toString());
			}
		}
		System.out.println("\n────────────────────────────────────────────────────────────────────────────────");
	}
	// =========================== List of borrowed books ========================	
	public static void borrowedBookList() {
		System.out.println("──────────────────────── List of borrowed books ────────────────────────────────\n");
		System.out.println("[ID\tMember\t\tBook\tIssue\t\tDue]");
		System.out.println("────────────────────────────────────────────────────────────────────────────────");
		for(Transaction transaction:library.getAllTransactions()) {
			System.out.println(transaction.toString());
		}
	}
	//checker data function
	public static String dataChecker(String id, String dataType) {
		boolean isDataExisted;
		if(dataType.equalsIgnoreCase("Member")) {
			isDataExisted = (library.getMember(id) != null);
		}else {
			isDataExisted = (library.getBook(id) != null);
		}
		if(isDataExisted) {
			System.out.println(dataType + " with ID " + id + " already exist, are you sure want to replace? (Y/N)");
			if(!scanner.nextLine().equalsIgnoreCase("y")) {
				id = null;
			}
		}
		return id;
	}
	//end of class
}
